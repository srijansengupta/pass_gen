import sequtils, algorithm, strutils, sugar, terminal, strformat, os

type AlphabetsRotationInt = range[1..25]

const atbash_cipher = "abcdefghijklmnopqrstuvwxyz".toSeq.reversed

func rotateWord*(pass: string, by: AlphabetsRotationInt): string =
  pass.map((ch) => (
    if not ch.isAlphaAscii: return ch
    elif ch.toLowerAscii.ord+by <= 'z'.ord: return (ch.ord + by.int).char
    else: return (ch.ord + by.int - 26).char
    )).join

func atbash*(pass: string): string =
  pass.map((ch) => (
    if not ch.isAlphaAscii: return ch
    elif ch.isLowerAscii: return atbash_cipher[1 + ch.ord - 'a'.ord]
    elif ch.isUpperAscii: return atbash_cipher[1 + ch.ord - 'A'.ord].toUpperAscii
    )).join

proc handleCtrlC(){.noconv.} =
  stdout.eraseScreen
  quit 0

when isMainModule:
  setControlCHook(handleCtrlC)

  var 
    repeatit = true
    by: AlphabetsRotationInt = 1
    atbashed = false
    pass = ""
    atbashAtFirst = true
    displayTime = 20
  while repeatit:
    styledEcho fgGreen, "Welcome to Password Generator..."
    stdout.styledWrite fgYellow, "Enter the Rotational number you want to rotate letters by..[1-25](default: 1) "
    try: by = stdin.readLine().parseInt().AlphabetsRotationInt
    except: 
      styledEcho fgRed, "Please enter number in range!!!"
      continue
    stdout.styledWrite fgYellow, "Would You like to use atbash?[y/n](default: n) "
    let choice = stdin.readLine()[0]
    if choice in {'Y', 'y'}: atbashed = true
    if atbashed: 
      stdout.styledWrite fgYellow, "What must be the sequence of appling atbash encryption?[1/2](default: 1) "
      let ch = stdin.readLine()[0]
      if ch == '2': atbashAtFirst = false
    stdout.styledWrite fgYellow, "For how long will you like to display the generated Password(in sec)? "
    try: displayTime = stdin.readLine().parseInt
    except:
      styledEcho fgRed, "Error!!!"
      continue
    stdout.styledWrite fgBlue, "Enter your Password: "
    if not readPasswordFromStdin("", pass):
      styledEcho fgRed, "Could not read a password."
      continue
    styledEcho fgGreen, "Generating Password", fmt""" with following info:
    Use atbash? {atbashed}
    rotate words by: {by}
    display time: {displayTime}
    atbashing at first? {atbashAtFirst}
    """
    stdout.writeLine "Entered Password:   ", pass
    if atbashed:
      if atbashAtFirst: pass = pass.atbash().rotateWord(by)
      else: pass = pass.rotateWord(by).atbash()
    else:
      pass = pass.rotateWord(by)
    stdout.writeLine "Generated password: ", pass
    sleep displayTime * 1000
    stdout.cursorUp 1
    stdout.eraseLine
    stdout.cursorUp 1
    stdout.eraseLine
    stdout.styledWrite fgYellow, "Would you like to generate password once again?(y/n) "
    let ch = stdin.readLine()[0]
    if ch in {'Y', 'y'}: repeatit = true
    else:  repeatit = false
