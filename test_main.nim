import unittest, main, algorithm

suite "Test Atbash encryption":
  test "abcdefghijklmnopqrstuvwxyz":
    check atbash("abcdefghijklmnopqrstuvwxyz")  == "abcdefghijklmnopqrstuvwxyz".reversed
  test "With special chars":
    check atbash("abc123") == ("zyx123")

suite "Test Rotational encryption":
  test "abcd with rotation 1":
    check "abcd".rotateWord(1) == "bcde"
  test "a with rotation 25":
    check rotateWord("a",25) == "z"
  test "c with rotation 25":
    check rotateWord("c", 25) == "b"
